﻿CREATE TABLE [dbo].[Company]
(
	[ID] INT NOT NULL PRIMARY KEY, 
    [Name] VARCHAR(200) NOT NULL, 
    [CUI] VARCHAR(20) NOT NULL, 
    [RegNumber] VARCHAR(20) NOT NULL, 
    [Address] VARCHAR(200) NULL, 
    [Phone] VARCHAR(20) NOT NULL, 
    [Mail] VARCHAR(50) NULL, 
    [IBAN] VARCHAR(20) NOT NULL, 
    [BankName] VARCHAR(30) NULL, 
    [CapitalAmount] NUMERIC(18, 3) NULL
)
