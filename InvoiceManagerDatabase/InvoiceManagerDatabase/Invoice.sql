﻿CREATE TABLE [dbo].[Invoice]
(
	[ID] INT NOT NULL PRIMARY KEY, 
    [Imported] BIT NOT NULL, 
    [InvoiceNumber] VARCHAR(20) NOT NULL, 
    [SerialNumber] VARCHAR(20) NOT NULL, 
    [DueDate] DATE NULL, 
    [Issuer] INT NOT NULL, 
    [BilledTo] INT NOT NULL, 
    [Paid] BIT NOT NULL, 
    [TotalAmountWOVAT] NUMERIC(5, 3) NOT NULL, 
    [TotalAmountVAT] NUMERIC(5, 3) NOT NULL, 
    [Printed] BIT NOT NULL, 
    [Type] VARCHAR(10) NOT NULL, 
    [LinkedInvoice] INT NOT NULL, 
    [CreatedAt] DATE NOT NULL, 
    CONSTRAINT [FK_Invoice_CompanyIssuer] FOREIGN KEY (Issuer) REFERENCES Company(ID),
    CONSTRAINT [FK_Invoice_CompanyBilledTo] FOREIGN KEY (BilledTo) REFERENCES Company(ID),
    CONSTRAINT [FK_Invoice_Invoice] FOREIGN KEY (LinkedInvoice) REFERENCES Invoice(ID)
)
