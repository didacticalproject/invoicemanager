﻿CREATE TABLE [dbo].[User]
(
	[ID] INT NOT NULL PRIMARY KEY, 
    [Username] VARCHAR(50) NOT NULL, 
    [Password] VARCHAR(30) NOT NULL,
    [Name] VARCHAR(100) NOT NULL, 
    [CNP] VARCHAR(20) NOT NULL, 
    [ICNumber] VARCHAR(10) NOT NULL, 
    [CompanyID] INT NOT NULL,
    [Role] VARCHAR(10) NOT NULL, 
    [Active] BIT NOT NULL, 
    [Mail] VARCHAR(50) NOT NULL, 
    [ProfileImage] VARCHAR(200) NULL, 
    CONSTRAINT [FK_User_Company] FOREIGN KEY (CompanyID) REFERENCES Company(ID) 
)
