﻿/*
Deployment script for InvoiceManagerDatabase_1

This code was generated by a tool.
Changes to this file may cause incorrect behavior and will be lost if
the code is regenerated.
*/

GO
SET ANSI_NULLS, ANSI_PADDING, ANSI_WARNINGS, ARITHABORT, CONCAT_NULL_YIELDS_NULL, QUOTED_IDENTIFIER ON;

SET NUMERIC_ROUNDABORT OFF;


GO
:setvar DatabaseName "InvoiceManagerDatabase_1"
:setvar DefaultFilePrefix "InvoiceManagerDatabase_1"
:setvar DefaultDataPath "C:\Users\iulia\AppData\Local\Microsoft\VisualStudio\SSDT\InvoiceManagerDatabase"
:setvar DefaultLogPath "C:\Users\iulia\AppData\Local\Microsoft\VisualStudio\SSDT\InvoiceManagerDatabase"

GO
:on error exit
GO
/*
Detect SQLCMD mode and disable script execution if SQLCMD mode is not supported.
To re-enable the script after enabling SQLCMD mode, execute the following:
SET NOEXEC OFF; 
*/
:setvar __IsSqlCmdEnabled "True"
GO
IF N'$(__IsSqlCmdEnabled)' NOT LIKE N'True'
    BEGIN
        PRINT N'SQLCMD mode must be enabled to successfully execute this script.';
        SET NOEXEC ON;
    END


GO
USE [$(DatabaseName)];


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET ARITHABORT ON,
                CONCAT_NULL_YIELDS_NULL ON,
                CURSOR_DEFAULT LOCAL 
            WITH ROLLBACK IMMEDIATE;
    END


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET PAGE_VERIFY NONE,
                DISABLE_BROKER 
            WITH ROLLBACK IMMEDIATE;
    END


GO
ALTER DATABASE [$(DatabaseName)]
    SET TARGET_RECOVERY_TIME = 0 SECONDS 
    WITH ROLLBACK IMMEDIATE;


GO
IF EXISTS (SELECT 1
           FROM   [master].[dbo].[sysdatabases]
           WHERE  [name] = N'$(DatabaseName)')
    BEGIN
        ALTER DATABASE [$(DatabaseName)]
            SET QUERY_STORE (CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 367)) 
            WITH ROLLBACK IMMEDIATE;
    END


GO
PRINT N'Rename refactoring operation with key 1d560633-49a9-4a4d-833a-3334b6e7271b is skipped, element [dbo].[User].[Id] (SqlSimpleColumn) will not be renamed to ID';


GO
PRINT N'Rename refactoring operation with key 092b37f3-1722-43be-9d0c-11b3c93c5d86 is skipped, element [dbo].[Company].[Id] (SqlSimpleColumn) will not be renamed to ID';


GO
PRINT N'Rename refactoring operation with key 8f3a2378-dd9e-4d27-b845-4d558eaefdf4 is skipped, element [dbo].[Product].[Id] (SqlSimpleColumn) will not be renamed to ID';


GO
PRINT N'Rename refactoring operation with key 845dbac6-e125-4748-9a69-cf92eddcfc68 is skipped, element [dbo].[Invoice].[Id] (SqlSimpleColumn) will not be renamed to ID';


GO
PRINT N'Rename refactoring operation with key 24348fe9-02b5-4dc9-9605-5ada37ac275d is skipped, element [dbo].[InvoiceProduct].[Id] (SqlSimpleColumn) will not be renamed to ID';


GO
PRINT N'Creating [dbo].[Company]...';


GO
CREATE TABLE [dbo].[Company] (
    [ID]            INT             NOT NULL,
    [Name]          VARCHAR (200)   NOT NULL,
    [CUI]           VARCHAR (20)    NOT NULL,
    [RegNumber]     VARCHAR (20)    NOT NULL,
    [Address]       VARCHAR (200)   NULL,
    [Phone]         VARCHAR (20)    NOT NULL,
    [Mail]          VARCHAR (50)    NULL,
    [IBAN]          VARCHAR (20)    NOT NULL,
    [BankName]      VARCHAR (30)    NULL,
    [CapitalAmount] NUMERIC (18, 3) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
PRINT N'Creating [dbo].[Invoice]...';


GO
CREATE TABLE [dbo].[Invoice] (
    [ID]               INT            NOT NULL,
    [Imported]         BIT            NOT NULL,
    [InvoiceNumber]    VARCHAR (20)   NOT NULL,
    [SerialNumber]     VARCHAR (20)   NOT NULL,
    [DueDate]          DATE           NULL,
    [Issuer]           INT            NOT NULL,
    [BilledTo]         INT            NOT NULL,
    [Paid]             BIT            NOT NULL,
    [TotalAmountWOVAT] NUMERIC (5, 3) NOT NULL,
    [TotalAmountVAT]   NUMERIC (5, 3) NOT NULL,
    [Printed]          BIT            NOT NULL,
    [Type]             VARCHAR (10)   NOT NULL,
    [LinkedInvoice]    INT            NOT NULL,
    [CreatedAt]        DATE           NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
PRINT N'Creating [dbo].[InvoiceProduct]...';


GO
CREATE TABLE [dbo].[InvoiceProduct] (
    [ID]        INT NOT NULL,
    [InvoiceID] INT NOT NULL,
    [ProductID] INT NOT NULL,
    [Quantity]  INT NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
PRINT N'Creating [dbo].[Product]...';


GO
CREATE TABLE [dbo].[Product] (
    [ID]         INT            NOT NULL,
    [Name]       VARCHAR (100)  NOT NULL,
    [Stock]      INT            NOT NULL,
    [PriceWOVAT] NUMERIC (5, 3) NOT NULL,
    [VAT]        NUMERIC (5, 3) NOT NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
PRINT N'Creating [dbo].[User]...';


GO
CREATE TABLE [dbo].[User] (
    [ID]           INT           NOT NULL,
    [Username]     VARCHAR (50)  NOT NULL,
    [Password]     VARCHAR (30)  NOT NULL,
    [Name]         VARCHAR (100) NOT NULL,
    [CNP]          VARCHAR (20)  NOT NULL,
    [ICNumber]     VARCHAR (10)  NOT NULL,
    [CompanyID]    INT           NOT NULL,
    [Role]         VARCHAR (10)  NOT NULL,
    [Active]       BIT           NOT NULL,
    [Mail]         VARCHAR (50)  NOT NULL,
    [ProfileImage] VARCHAR (200) NULL,
    PRIMARY KEY CLUSTERED ([ID] ASC)
);


GO
PRINT N'Creating [dbo].[FK_Invoice_CompanyIssuer]...';


GO
ALTER TABLE [dbo].[Invoice] WITH NOCHECK
    ADD CONSTRAINT [FK_Invoice_CompanyIssuer] FOREIGN KEY ([Issuer]) REFERENCES [dbo].[Company] ([ID]);


GO
PRINT N'Creating [dbo].[FK_Invoice_CompanyBilledTo]...';


GO
ALTER TABLE [dbo].[Invoice] WITH NOCHECK
    ADD CONSTRAINT [FK_Invoice_CompanyBilledTo] FOREIGN KEY ([BilledTo]) REFERENCES [dbo].[Company] ([ID]);


GO
PRINT N'Creating [dbo].[FK_Invoice_Invoice]...';


GO
ALTER TABLE [dbo].[Invoice] WITH NOCHECK
    ADD CONSTRAINT [FK_Invoice_Invoice] FOREIGN KEY ([LinkedInvoice]) REFERENCES [dbo].[Invoice] ([ID]);


GO
PRINT N'Creating [dbo].[FK_InvoiceProduct_Invoice]...';


GO
ALTER TABLE [dbo].[InvoiceProduct] WITH NOCHECK
    ADD CONSTRAINT [FK_InvoiceProduct_Invoice] FOREIGN KEY ([InvoiceID]) REFERENCES [dbo].[Invoice] ([ID]);


GO
PRINT N'Creating [dbo].[FK_InvoiceProduct_Product]...';


GO
ALTER TABLE [dbo].[InvoiceProduct] WITH NOCHECK
    ADD CONSTRAINT [FK_InvoiceProduct_Product] FOREIGN KEY ([ProductID]) REFERENCES [dbo].[Product] ([ID]);


GO
PRINT N'Creating [dbo].[FK_User_Company]...';


GO
ALTER TABLE [dbo].[User] WITH NOCHECK
    ADD CONSTRAINT [FK_User_Company] FOREIGN KEY ([CompanyID]) REFERENCES [dbo].[Company] ([ID]);


GO
-- Refactoring step to update target server with deployed transaction logs

IF OBJECT_ID(N'dbo.__RefactorLog') IS NULL
BEGIN
    CREATE TABLE [dbo].[__RefactorLog] (OperationKey UNIQUEIDENTIFIER NOT NULL PRIMARY KEY)
    EXEC sp_addextendedproperty N'microsoft_database_tools_support', N'refactoring log', N'schema', N'dbo', N'table', N'__RefactorLog'
END
GO
IF NOT EXISTS (SELECT OperationKey FROM [dbo].[__RefactorLog] WHERE OperationKey = '1d560633-49a9-4a4d-833a-3334b6e7271b')
INSERT INTO [dbo].[__RefactorLog] (OperationKey) values ('1d560633-49a9-4a4d-833a-3334b6e7271b')
IF NOT EXISTS (SELECT OperationKey FROM [dbo].[__RefactorLog] WHERE OperationKey = '092b37f3-1722-43be-9d0c-11b3c93c5d86')
INSERT INTO [dbo].[__RefactorLog] (OperationKey) values ('092b37f3-1722-43be-9d0c-11b3c93c5d86')
IF NOT EXISTS (SELECT OperationKey FROM [dbo].[__RefactorLog] WHERE OperationKey = '8f3a2378-dd9e-4d27-b845-4d558eaefdf4')
INSERT INTO [dbo].[__RefactorLog] (OperationKey) values ('8f3a2378-dd9e-4d27-b845-4d558eaefdf4')
IF NOT EXISTS (SELECT OperationKey FROM [dbo].[__RefactorLog] WHERE OperationKey = '845dbac6-e125-4748-9a69-cf92eddcfc68')
INSERT INTO [dbo].[__RefactorLog] (OperationKey) values ('845dbac6-e125-4748-9a69-cf92eddcfc68')
IF NOT EXISTS (SELECT OperationKey FROM [dbo].[__RefactorLog] WHERE OperationKey = '24348fe9-02b5-4dc9-9605-5ada37ac275d')
INSERT INTO [dbo].[__RefactorLog] (OperationKey) values ('24348fe9-02b5-4dc9-9605-5ada37ac275d')

GO

GO
PRINT N'Checking existing data against newly created constraints';


GO
USE [$(DatabaseName)];


GO
ALTER TABLE [dbo].[Invoice] WITH CHECK CHECK CONSTRAINT [FK_Invoice_CompanyIssuer];

ALTER TABLE [dbo].[Invoice] WITH CHECK CHECK CONSTRAINT [FK_Invoice_CompanyBilledTo];

ALTER TABLE [dbo].[Invoice] WITH CHECK CHECK CONSTRAINT [FK_Invoice_Invoice];

ALTER TABLE [dbo].[InvoiceProduct] WITH CHECK CHECK CONSTRAINT [FK_InvoiceProduct_Invoice];

ALTER TABLE [dbo].[InvoiceProduct] WITH CHECK CHECK CONSTRAINT [FK_InvoiceProduct_Product];

ALTER TABLE [dbo].[User] WITH CHECK CHECK CONSTRAINT [FK_User_Company];


GO
PRINT N'Update complete.';


GO
