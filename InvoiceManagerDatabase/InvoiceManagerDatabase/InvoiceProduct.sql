﻿CREATE TABLE [dbo].[InvoiceProduct]
(
	[ID] INT NOT NULL PRIMARY KEY, 
    [InvoiceID] INT NOT NULL, 
    [ProductID] INT NOT NULL, 
    [Quantity] INT NOT NULL, 
    CONSTRAINT [FK_InvoiceProduct_Invoice] FOREIGN KEY (InvoiceID) REFERENCES Invoice(ID), 
    CONSTRAINT [FK_InvoiceProduct_Product] FOREIGN KEY (ProductID) REFERENCES Product(ID)
)
