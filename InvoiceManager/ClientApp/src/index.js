import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter as Router, Switch, Route, Redirect} from 'react-router-dom';
import './custom.css'
import LogIn from './components/LogIn';
import User from './components/User';
import Company from './components/Company';
import Invoice from './components/Invoice';
import Home from './components/Home';
import Routes from './components/Routes';
import {useRoutes, A} from 'hookrouter';

const App = () => {

    const chosenRoute = useRoutes(Routes);

    return (
        chosenRoute
    );
}

ReactDOM.render(<App/>, document.getElementById('root'));