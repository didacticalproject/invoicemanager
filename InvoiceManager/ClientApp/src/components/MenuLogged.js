import React from 'react';
import Routes from './Routes';
import {useRoutes, A} from 'hookrouter';

const MenuLogged = (props) => {

    const chosenRoute = useRoutes(Routes);

    return (
        <div id="menu" fluid="md">
                <div className="ui secondary pointing menu">
                    <img className="item" src="https://dige2.com/demo/digibirds/p02/resources/website/common/images/home/slide2_circle_inner_blue.png"/>
                    <A className="item" href="/dashboard">Dashboard</A>
                    <A className="item" href="/mails">Mails</A>
                    <A className="item" href="/users">Users</A>
                    <A className="item" href="/companies">Companies</A>
                    <A className="item" href="/products">Products</A>
                    <A className="item" href="/invoices">Invoices</A>
                    <div className="right menu">
                        <A className="item" href="/login">My profile</A>
                        <A className="item" href="/home">Log out</A>
                    </div>
                </div>
        </div>
    );
}

export default MenuLogged;