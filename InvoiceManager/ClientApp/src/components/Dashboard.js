import React from 'react';
import LabelledInput from './LabelledInput';
import MenuLogged from './MenuLogged';

const styleLabel = {
    width: "100px",
    fontSize: "20px",
    marginLeft: "50px"
}

const styleInput = {
    fontSize: "15px",
    backgroundColor: "#fafafa"
}

const styleSelection = {
    fontWeight: "bold",
    maxWidth: "250px",
    width: "150px",
    marginLeft: "50px",
    fontSize: "22px",
}

const styleCheckbox = {
    width: "25px",
    height: "25px",
    backgroundColor: "red"
}

const Dashboard = () => {
    return (
        <div fluid="md">
            <MenuLogged/>
            <table className="ui celled table">
                <thead>
                    <tr>
                        <th>Username</th>
                        <th>Name</th>
                        <th>Personal numeric code</th>
                        <th>Identity card number</th>
                        <th>Role</th>
                        <th>Mail</th>
                        <th>Company</th>
                        <th>Active</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <td data-label="Name">James</td>
                    <td data-label="Age">24</td>
                    <td data-label="Job">Engineer</td>
                    </tr>
                    <tr>
                    <td data-label="Name">Jill</td>
                    <td data-label="Age">26</td>
                    <td data-label="Job">Engineer</td>
                    </tr>
                    <tr>
                    <td data-label="Name">Elyse</td>
                    <td data-label="Age">24</td>
                    <td data-label="Job">Designer</td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}

export default Dashboard;