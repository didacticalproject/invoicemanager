import React from 'react';
import LabelledInput from './LabelledInput';
import MenuLogged from './MenuLogged';

const styleLabel = {
    width: "100px",
    fontSize: "20px",
    marginLeft: "50px"
}

const styleInput = {
    fontSize: "15px",
    backgroundColor: "#fafafa"
}

const styleSelection = {
    fontWeight: "bold",
    maxWidth: "250px",
    width: "150px",
    marginLeft: "50px",
    fontSize: "22px",
}

const styleCheckbox = {
    width: "25px",
    height: "25px",
    backgroundColor: "red"
}

const Company = () => {
    return (
        <div fluid="md">
            <MenuLogged/>
            <h1>Company Management</h1>
            <div className="d-flex flex-row flex-wrap justify-content-center">
                <div class="d-flex flex-column">
                    <LabelledInput styleLabel={styleLabel} styleInput={styleInput} label="UIC" type="text" id="UIC"/>
                    <LabelledInput styleLabel={styleLabel} styleInput={styleInput} label="Name" type="text" id="Name"/>
                </div>
                <div class="d-flex flex-column">
                    <LabelledInput styleLabel={styleLabel} styleInput={styleInput} label="Register no." type="text" id="Register no."/>
                    <LabelledInput styleLabel={styleLabel} styleInput={styleInput} label="Address" type="text" id="Address"/>
                </div>
                <div class="d-flex flex-column">
                    <LabelledInput styleLabel={styleLabel} styleInput={styleInput} label="Phone" type="text" id="Phone"/>
                    <LabelledInput styleLabel={styleLabel} styleInput={styleInput} label="Mail" type="text" id="Mail"/>
                </div>
                <div class="d-flex flex-column">
                    <LabelledInput styleLabel={styleLabel} styleInput={styleInput} label="IBAN" type="text" id="IBAN"/>
                    <LabelledInput styleLabel={styleLabel} styleInput={styleInput} label="Bank name" type="text" id="Bank name"/>
                </div>
            </div>
            <div className="container row">
                <div className="col-7 col-sm-5 d-flex justify-content-start">
                    <LabelledInput styleLabel={styleSelection} styleInput={styleCheckbox} label="Edit mode" type="checkbox" id="Edit mode"/>
                </div>
                {/* <div className="d-flex justify-content-center align-self-start"> */}
                <div className="col">
                    <LabelledInput styleLabel={styleSelection} styleInput={styleCheckbox} label="Matches all filters" type="checkbox" id="Matches all filters"/>
                </div>
                <div className="col">
                    <LabelledInput styleLabel={styleSelection} styleInput={styleCheckbox} label="Matches one filter" type="checkbox" id="Matches one filter"/>
                </div>

            </div>
            <table className="ui celled table">
                <thead>
                    <tr>
                        <th>UIC</th>
                        <th>Name</th>
                        <th>Register number</th>
                        <th>Address</th>
                        <th>Phone</th>
                        <th>Mail</th>
                        <th>IBAN</th>
                        <th>Bank name</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                    <td data-label="Name">James</td>
                    <td data-label="Age">24</td>
                    <td data-label="Job">Engineer</td>
                    </tr>
                    <tr>
                    <td data-label="Name">Jill</td>
                    <td data-label="Age">26</td>
                    <td data-label="Job">Engineer</td>
                    </tr>
                    <tr>
                    <td data-label="Name">Elyse</td>
                    <td data-label="Age">24</td>
                    <td data-label="Job">Designer</td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}

export default Company;