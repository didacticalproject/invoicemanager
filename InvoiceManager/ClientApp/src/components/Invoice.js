import React from 'react';
import LabelledInput from './LabelledInput';
import MenuLogged from './MenuLogged';

const styleLabel = {
    width: "100px",
    fontSize: "20px",
    marginLeft: "50px"
}

const styleInput = {
    fontSize: "15px",
    backgroundColor: "#fafafa"
}

const styleSelection = {
    fontWeight: "bold",
    maxWidth: "250px",
    width: "150px",
    marginLeft: "50px",
    fontSize: "22px",
}

const styleCheckbox = {
    width: "25px",
    height: "25px",
    backgroundColor: "red"
}

const Invoice = () => {
    return (
        <div fluid="md">
            <MenuLogged/>
            <h1>Invoice Management</h1>
            <div className="d-flex flex-row flex-wrap justify-content-center">
                <div class="d-flex flex-column">
                    <LabelledInput styleLabel={styleLabel} styleInput={styleInput} label="Issuer" type="text" id="Issuer"/>
                    <LabelledInput styleLabel={styleLabel} styleInput={styleInput} label="Billed to" type="text" id="Billed to"/>
                </div>
                <div class="d-flex flex-column">
                    <LabelledInput styleLabel={styleLabel} styleInput={styleInput} label="Invoice no." type="text" id="Invoice no."/>
                    <LabelledInput styleLabel={styleLabel} styleInput={styleInput} label="Serial no." type="text" id="Serial no."/>
                </div>
                <div class="d-flex flex-column">
                    <LabelledInput styleLabel={styleLabel} styleInput={styleInput} label="Paid" type="text" id="Paid"/>
                    <LabelledInput styleLabel={styleLabel} styleInput={styleInput} label="Printed" type="text" id="Printed"/>
                </div>
                <div class="d-flex flex-column">
                    <LabelledInput styleLabel={styleLabel} styleInput={styleInput} label="Type" type="text" id="Type"/>
                    <LabelledInput styleLabel={styleLabel} styleInput={styleInput} label="Created at" type="text" id="Created at"/>
                </div>
            </div>
            <div className="container row">
                <div className="col-7 col-sm-5 d-flex justify-content-start">
                    <LabelledInput styleLabel={styleSelection} styleInput={styleCheckbox} label="Edit mode" type="checkbox" id="Edit mode"/>
                </div>
                {/* <div className="d-flex justify-content-center align-self-start"> */}
                <div className="col">
                    <LabelledInput styleLabel={styleSelection} styleInput={styleCheckbox} label="Matches all filters" type="checkbox" id="Matches all filters"/>
                </div>
                <div className="col">
                    <LabelledInput styleLabel={styleSelection} styleInput={styleCheckbox} label="Matches one filter" type="checkbox" id="Matches one filter"/>
                </div>

            </div>
            <table class="ui celled structured table">
                <thead>
                    <tr>
                        <th>Issuer</th>
                        <th>Billed to</th>
                        <th>Created at</th>
                        <th>Invoice no.</th>
                        <th>Serial no.</th>
                        <th>Type</th>
                        <th>Printed</th>
                        <th>Paid</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Alpha Team</td>
                        <td>Project 1</td>
                        <td class="right aligned">2</td>
                        <td class="center aligned">
                            <i class="large green checkmark icon"></i>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td rowspan="3">Beta Team</td>
                        <td>Project 1</td>
                        <td class="right aligned">52</td>
                        <td class="center aligned">
                            <i class="large green checkmark icon"></i>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Project 2</td>
                        <td class="right aligned">12</td>
                        <td></td>
                        <td class="center aligned">
                            <i class="large green checkmark icon"></i>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>Project 3</td>
                        <td class="right aligned">21</td>
                        <td class="center aligned">
                            <i class="large green checkmark icon"></i>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>
        </div>
    );
}

export default Invoice;