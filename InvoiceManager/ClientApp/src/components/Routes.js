import React from 'react';
import LogIn from './LogIn';
import User from './User';
import Company from './Company';
import Invoice from './Invoice';
import Home from './Home';
import Dashboard from './Dashboard';

const routes = {
    "/": () => <Home />,
    "/home": () => <Home/>,
    "/login": () => <LogIn/>,
    "/dashboard": () => <Dashboard/>,
    "/mails": () => <User/>,
    "/users": () => <User/>,
    "/companies": () => <Company/>,
    "/products": () => <Company/>,
    "/invoices": () => <Invoice/>
  };
  export default routes;