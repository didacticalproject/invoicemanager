import React from 'react';

const LabelledInput = (props) => {
    return (
        <form className="d-flex justify-content-center">
            <label style={props.styleLabel} htmlFor={props.id}>{props.label}</label>
            <input style={props.styleInput} type={props.type} id={props.id}></input>
        </form>
    );
}

export default LabelledInput;