import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import Routes from './Routes';
import {useRoutes, A} from 'hookrouter';
import { useState } from 'react';

const MenuIntro = (props) => {
    const chosenRoute = useRoutes(Routes);
    const [active, setActive] = useState(0);
    const path=window.location.href.split('/')[3];

    return (
        <div id="menu" fluid="md">
            <Router>
                <div className="ui secondary pointing menu">
                    <img className="item" src="https://dige2.com/demo/digibirds/p02/resources/website/common/images/home/slide2_circle_inner_blue.png"/>
                    <A className={"item " + (path==="home"? "active":"")} href="/home" onClick={() => setActive(0)}>
                        Home
                    </A>
                    <div className="right menu">
                        <A className={"item " + (path==="login"? "active":"")}  href="/login" onClick={() => setActive(1)}>
                            Log in
                        </A>
                    </div>
                    
                </div>
            </Router>
            {console.log(window.location.href.split('/')[3])}
        </div>
    );
}

export default MenuIntro;