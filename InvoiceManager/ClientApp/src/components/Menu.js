import React from 'react';
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';
import LogIn from './LogIn';
import User from './User';

const Menu = (props) => {
    return (
        <div id="menu" fluid="md">
            <Router>
                <div className="ui secondary pointing menu">
                    <a className="active item" href="/home">
                        Home
                    </a>
                    <a className="item" href="/mails">
                        Mails
                    </a>
                    <a className="item" href="/users">
                        Users
                    </a>
                    <a className="item" href="/companies">
                        Companies
                    </a>
                    <a className="item" href="/products">
                        Products
                    </a>
                    <a className="item" href="/invoices">
                        Invoices
                    </a>
                    <a className="item" href="/statistics">
                        Statistics
                    </a>
                    <div className="right menu">
                        <a className="ui item" href="/login">
                            Logout
                        </a>
                    </div>
                </div>
                {/* <Switch>
                    <Route path="/mails">
                        <LogIn/>
                    </Route>
                    <Route path="/users">
                        <User/>
                    </Route>
                    <Route path="/companies">
                        <LogIn/>
                    </Route>
                    <Route path="/products">
                        <LogIn/>
                    </Route>
                    <Route path="/invoices">
                        <LogIn/>
                    </Route>
                    <Route path="/statistics">
                        <LogIn/>
                    </Route>
                    <Route path="/login">
                        <LogIn/>
                    </Route>
                </Switch> */}
            </Router>
        </div>
    );
}

export default Menu;