import React from 'react';
import MenuIntro from './MenuIntro';

const Home = () => {
    return (
        <div fluid="md">
            <MenuIntro/>
            <img src="https://i.pinimg.com/originals/4a/94/26/4a94268541d7a0ed95a8be5138e8a288.jpg"/>
            <h1 style={{position: "absolute", color: "white", top: "50%", left: "50%", transform: "translate(-50%, -150%)"}}>BILL & STAMPED INC</h1>
        </div>
    );
}

export default Home;