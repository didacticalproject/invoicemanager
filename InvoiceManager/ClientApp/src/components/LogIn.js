import React from 'react';
import LabelledInput from './LabelledInput';
import {useHistory, Route} from 'react-router-dom';
import MenuIntro from './MenuIntro';
import Routes from './Routes';
import {useRoutes, A} from 'hookrouter';

const styleForm = {
    paddingTop:"30px",
    border: "2px solid black",
    borderRadius:"10px",
    paddingBottom: "30px",
    width: "500px",
    background: "#fff"
}


const LogIn = () => {
    const chosenRoute = useRoutes(Routes);

    return (
        <div fluid="md">
            <MenuIntro/>
            <h1>Log in</h1>
            <div className="d-flex justify-content-center">
                <div className="d-flex flex-column" style={styleForm}>
                    <LabelledInput label="Username" type="text" id="Username"/>
                    <br/>
                    <LabelledInput label="Password" type="password" id="Password"/>
                    <br/>
                    <button type="submit"><A href="/dashboard">Submit</A></button>
                </div>
            </div>
        </div>
    );
}

export default LogIn;